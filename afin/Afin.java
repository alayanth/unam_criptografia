import java.util.*;
import java.math.*;

class Afin {

    public static void main(String[] args) {
    
    String Mi = new String();
    String clave = new String();
    String cipher = new String();
    Scanner lector = new Scanner(System.in);
    int option;
    int a;
    int b;
    
    Modulo mod = new Modulo();
    Afin afin = new Afin();
    Affine affine = new Affine();
    System.out.println("Universidad Nacional Autónoma de México");
    System.out.println("\tFacultad de Ingenieria");
    System.out.println("   Criptografía Cifrador Afín");
    
    System.out.println("Selecciona la opción que deseas ejecutar:\n"
			  +"1) Cifrar con Ci = (Mi + k)mod37.\n"
			  +"2) Descifrar (Mi + k)mod37.\n"
			  +"3) Cifrar con Ci = (a*Mi)mod37.\n"
			  +"4) Descifrar (a*Mi)mod37.\n"
			  +"5) Cifrar con Ci = (a*Mi + b)mod37.\n"
			  +"6) Descifrar (a*Mi + k)mod37.\n");
			
    option = lector.nextInt();
    lector.nextLine();

	
    switch(option){
      case 1:
	System.out.println("Por favor introduce el mensaje a cifrar SIN ESPACIOS:");
	Mi = lector.nextLine();
	Mi = Mi.toLowerCase();
	
	System.out.println("Ahora la clave para cifrar:");
	clave = lector.nextLine();
	clave = clave.toLowerCase();
	
	for(int k = 0; clave.length() < Mi.length(); k++){
		clave = clave.concat(clave);
	}
	System.out.println("Cifrando con Ci = (Mi + k)modn: " + affine.cifrar1(Mi,clave));
	break;
      case 2:
	System.out.println("Por favor introduce el mensaje cifrado:");
	cipher = lector.nextLine();
	cipher = cipher.toLowerCase();
	
	System.out.println("Ahora la clave para descifrar:");
	clave = lector.nextLine();
	clave = clave.toLowerCase();
	
	for(int k = 0; clave.length() < cipher.length(); k++){
		clave = clave.concat(clave);
	}
	System.out.println("Descifrando Mi = : " + affine.dcifrar1(cipher,clave));
	break;
      case 3:
	System.out.println("Por favor introduce el mensaje a cifrar SIN ESPACIOS:");
	Mi = lector.nextLine();
	Mi = Mi.toLowerCase();
	
	System.out.println("Ahora la constante de decimación (a):");
	a = lector.nextInt();
	lector.nextLine();
	
	System.out.println("Cifrando con Ci = (a*Mi)modn: " + affine.cifrar2(a,Mi));
	
	break;
      case 4:
	System.out.println("Por favor introduce el mensaje cifrado:");
	cipher = lector.nextLine();
	cipher = cipher.toLowerCase();
	
	System.out.println("Ahora la constante de decimación (a):");
	a = lector.nextInt();
	lector.nextLine();
	
	System.out.println("Descifrando Mi = : " + affine.dcifrar2(a,cipher));
	break;
      case 5:
	System.out.println("Por favor introduce el mensaje a cifrar SIN ESPACIOS:");
	Mi = lector.nextLine();
	Mi = Mi.toLowerCase();
	
	System.out.println("Ahora la clave para cifrar:");
	clave = lector.nextLine();
	clave = clave.toLowerCase();
	
	for(int k = 0; clave.length() < Mi.length(); k++){
		clave = clave.concat(clave);
	}
	System.out.println("Y la constante de decimación (a)");
	a = lector.nextInt();
	lector.nextLine();

	System.out.println("Cifrando con Ci = (a*Mi + k)modn: " + affine.cifrar3(a,Mi,clave));
	break;
      case 6:
	System.out.println("Por favor introduce el mensaje cifrado:");
	cipher = lector.nextLine();
	cipher = cipher.toLowerCase();
	
	System.out.println("Ahora la clave para descifrar:");
	clave = lector.nextLine();
	clave = clave.toLowerCase();
	
	for(int k = 0; clave.length() < cipher.length(); k++){
		clave = clave.concat(clave);
	}
	System.out.println("Y la constante de decimación (a)");
	a = lector.nextInt();
	lector.nextLine();
	
	System.out.println("Descifrando Mi = " + affine.dcifrar3(a,cipher,clave));
	break;
      default:
	System.out.println("Opción incorrecta");
	break;
    }
//     System.out.println("Cifrando con Ci = (Mi + k)modn: " + affine.cifrar1("osa","pez"));
//     System.out.println("Descifrando Mi = : " + affine.dcifrar1("4wz","pez"));
//     
//     System.out.println("Cifrando con Ci = (a*Mi)modn: " + affine.cifrar2(5,"hola"));
//     System.out.println("Descifrando: " + affine.dcifrar2(5,"8bra"));
//     
//     System.out.println("Cifrando con Ci = (a*Mi + k)modn: " + affine.cifrar3(4, "bambiesunvenado", "ruvalcabadelosmontes"));
//     System.out.println("Decifrando: " + affine.dcifrar3(4,"vu6egrcloqtzo48", "ruvalcabadelosmontes"));
//   
  }
}
class Affine{
  //Ci = (a*Mi+b)mod n
  Hashtable <String, Integer> hashAlpha2Num = new Hashtable <String, Integer>(){{ 
      put("a",0);  
      put("b",1);
      put("c",2);
      put("d",3);
      put("e",4);
      put("f",5);
      put("g",6);
      put("h",7);
      put("i",8);
      put("j",9);
      put("k",10);
      put("l",11);
      put("m",12);
      put("n",13);
      put("ñ",14);
      put("o",15);
      put("p",16);
      put("q",17);
      put("r",18);
      put("s",19);
      put("t",20);
      put("u",21);
      put("v",22);
      put("w",23);
      put("x",24);
      put("y",25);
      put("z",26);
      put("0",27);
      put("1",28);
      put("2",29);
      put("3",30);
      put("4",31);
      put("5",32);
      put("6",33);
      put("7",34);
      put("8",35);
      put("9",36); }};
      
  Hashtable <Integer, String> hashNum2Alpha = new Hashtable <Integer, String>(){{ 
      put(0,"a");
      put(1,"b");
      put(2,"c");
      put(3,"d");
      put(4,"e");
      put(5,"f");
      put(6,"g");
      put(7,"h");
      put(8,"i");
      put(9,"j");
      put(10,"k");
      put(11,"l");
      put(12,"m");
      put(13,"n");
      put(14,"ñ");
      put(15,"o");
      put(16,"p");
      put(17,"q");
      put(18,"r");
      put(19,"s");
      put(20,"t");
      put(21,"u");
      put(22,"v");
      put(23,"w");
      put(24,"x");
      put(25,"y");
      put(26,"z");
      put(27,"0");
      put(28,"1");
      put(29,"2");
      put(30,"3");
      put(31,"4");
      put(32,"5");
      put(33,"6");
      put(34,"7");
      put(35,"8");
      put(36,"9"); }};
  
  private int a;	//constante de decimación
  private String Mi;	//Mensaje en claro
  private int b;	//constante de dezplazamiento
  private int n = 37;	//Número de simbolos en el alfabeto
  private String cipher;  //Mensaje cifrado
  private String clave;   //clave para cifrar o descifrar
  
  public void setCtedeDecimacion(int ctedec){
      a = ctedec;
  }
  
  public void setCtedeDezplazamiento(int ctedez){
      b = ctedez;
  }
  
  public void setMi(String mensaje){
      Mi = mensaje;
  }
  
  public void setClave(String key){
      clave = key;
  }
  
  public String getClave(){
      return clave;
  }
  
  public String getMi(){
      return Mi;
  }
  
  public void setAlfa(int num){
      n = num;
  }
  
  public int getAlfa(){
      return n;
  }
  
  public String cifrar1(String Mi, String clave){
      //Ci = (Mi+K)mod n
      Modulo mod = new Modulo();
      StringBuilder srtBuilder = new StringBuilder();
  
      for(int i = 0; i < Mi.length(); i++){
	  Character c =  Mi.charAt(i);
	  Character d = clave.charAt(i);
	  int res;
	  String e = "";
	  
 	  res = (hashAlpha2Num.get(Character.toString(c)) + hashAlpha2Num.get(Character.toString(d)));
 	  e = hashNum2Alpha.get(mod.calcular(res,n));
 	  srtBuilder.append(e);
      }
      cipher = srtBuilder.toString();
      return cipher;
      
  }
  
  public String dcifrar1(String cipher, String clave){
      Modulo mod = new Modulo();
      StringBuilder srtBuilder = new StringBuilder();
 
      for(int i = 0; i < cipher.length(); i++){
	  Character c =  cipher.charAt(i);
	  Character d = clave.charAt(i);
	  int res;
	  String e = "";
	  
 	  res = (hashAlpha2Num.get(Character.toString(c)) - hashAlpha2Num.get(Character.toString(d)));
 	  if(res < 0){
	    int u = 0;
	    while(res < 0){
	      res = res + n*u;
	      u++;
	    }
 	  }
 	  e = hashNum2Alpha.get(mod.calcular(res,n));
 	  srtBuilder.append(e);
      }
      mensaje = srtBuilder.toString();
      return mensaje;
  }
  
  public String cifrar2(int a, String Mi){
      Modulo mod = new Modulo();
      StringBuilder srtBuilder = new StringBuilder();
      
      for(int i = 0; i < Mi.length(); i++){
	  Character c =  Mi.charAt(i);
	  int res;
	  String e = "";
	  
 	  res = (hashAlpha2Num.get(Character.toString(c)) * a);
 	  if(res < 0){
	    int u = 0;
	    while(res < 0){
	      res = res + n*u;
	      u++;
	    }
 	  }
 	  e = hashNum2Alpha.get(mod.calcular(res,n));
 	  srtBuilder.append(e);
      }
      cipher = srtBuilder.toString();
      return cipher;
      
  }
  
  public String dcifrar2(Integer a, String cipher){
      Modulo mod = new Modulo();
      StringBuilder srtBuilder = new StringBuilder();
      String astring = Integer.toString(a);
      String cstring = Integer.toString(n);
      
      for(int i = 0; i < cipher.length(); i++){
	  BigInteger bi1 = new BigInteger(astring);
	  BigInteger bi2 = new BigInteger(cstring);
	  BigInteger bi3 = bi1.modInverse(bi2);
	  
	  Character c =  cipher.charAt(i);
	  int res;
	  int inv = bi3.intValue();
	  String e = "";
	  
 	  res = (hashAlpha2Num.get(Character.toString(c)) * inv);
 	  e = hashNum2Alpha.get(mod.calcular(res,n));
 	  srtBuilder.append(e);
      }
      mensaje = srtBuilder.toString();
      return mensaje;
  }

  public String cifrar3(int a, String Mi, String clave){
      //Ci = (a*Mi + d) mod n
      Modulo mod = new Modulo();
      StringBuilder srtBuilder = new StringBuilder();
      
      for(int i = 0; i < Mi.length(); i++){
	  Character c =  Mi.charAt(i);
	  Character d = clave.charAt(i);
	  int res;
	  String e = "";
	  
 	  res = (a * hashAlpha2Num.get(Character.toString(c))) + hashAlpha2Num.get(Character.toString(d));
 	  e = hashNum2Alpha.get(mod.calcular(res,n));
 	  srtBuilder.append(e);
      }
      cipher = srtBuilder.toString();
      return cipher;
  }
  
  public String dcifrar3(Integer a, String cipher, String clave){
      Modulo mod = new Modulo();
      StringBuilder srtBuilder = new StringBuilder();
      String astring = Integer.toString(a);
      String cstring = Integer.toString(n);
      
      for(int i = 0; i < cipher.length(); i++){
      
	  BigInteger bi1 = new BigInteger(astring);
	  BigInteger bi2 = new BigInteger(cstring);
	  BigInteger bi3 = bi1.modInverse(bi2);
	  
	  Character c = cipher.charAt(i);
	  Character d = clave.charAt(i);
	  int res;
	  int inv = bi3.intValue();
	  String e = "";
	 
 	  res = inv * (hashAlpha2Num.get(Character.toString(c)) - hashAlpha2Num.get(Character.toString(d)));
 	  if(res < 0){
	    int u = 0;
	    while(res < 0){
	      res = res + n*u;
	      u++;
	    }
 	  }
 	  e = hashNum2Alpha.get(mod.calcular(res,n));
 	  srtBuilder.append(e);
      }
      mensaje = srtBuilder.toString();
      return mensaje;
  }
}


class Modulo{
  private int dividendo;
  private int divisor;
  private int resultado;
  
  public void setDividendo(int divo){
      dividendo = divo;
  }
  
  public void setDivisor(int divi){
      divisor = divi;
  }
  
  public int calcular(int divo,int divi){
      dividendo = divo;
      divisor = divi;
      
      resultado = divo % divi;
      return resultado;
  }
}
