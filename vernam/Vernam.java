import java.lang.Math;
import java.util.Scanner;

public class Vernam {
	public static void main(String args[]) {
	    Scanner lector = new Scanner(System.in);
	    String Mi = new String();
	    String clave = new String();
	    String cipher = new String();
	    int option;
	    
	    CVernam cifrador = new CVernam();
	    DVernam descifrador = new DVernam();
	    
	    System.out.println("Universidad Nacional Autónoma de México\n"
			      +"\tFacultad de Ingenería\n"
			      +"\t     Criptografía\n"
			      +"\t  Cifrador Vernam");
			      
	    System.out.println("Seleccione la opción que desea realizar: \n"
			      +"1) Cifrar\n"
			      +"2) Descifrar");
	    
	    option = lector.nextInt();
	    
	    switch(option){
	      case 1:
		  cifrador.cifrar();
		break;
	      case 2:
		  descifrador.descifrar();
		break;
	      default:
		  System.out.println("Opción incorrecta");
		break;
	    }
	    
// 	    System.out.println("Introduzca el mensaje a cifrar:");
// 	    Mi = lector.nextLine();
// 	    System.out.println("Introduzca la clave para cifrar:");
// 	    clave = lector.nextLine();
// 	    
// 	    for(int k = 0; k < 3; k++){
// 		clave = clave.concat(clave);
// 	    }
// 	    
// 	    // This would be the text we encrypt (in this case "hello")
// 	    // We convert it to a character array
// 	    char[] arText = Mi.toCharArray();
// 
// 	    // This would be our vernam cipher (should be same length as our text)
// 	    // Here we use the same letters, but theoretically should be random
// 	    // characters generated on the fly. USE RANDOM LETTERS!;
// 	    char[] arCipher = clave.toCharArray();
// 
// 	    // Array to hold our encryption (again same length)
// 	    char[] encoded = new char[Mi.length()];
// 
// 	    // Encrypt the text by using XOR (exclusive OR) each character
// 	    // of our text against cipher.
// 	    System.out.println("Cifrando " + Mi + " a: ");
// 	    for (int i = 0; i < arText.length; i++) {
// 		    encoded[i] =  (char) (arText[i] ^ arCipher[i]);
// 		    System.out.print(encoded[i]);
// 	    }
// 
// 	    System.out.println("\nDescifrando el texto es: ");
// 
// 	    // Run through the encrypted text and against the cipher again
// 	    // This decrypts the text.
// 	    for (int i = 0; i < encoded.length; i++) {
// 		    char temp = (char) (encoded[i] ^ arCipher[i]);
// 		    System.out.print(temp);
// 	    }
// 	    System.out.println();
	 }
}

class CVernam{
      private String Mi;
      private String clave;
      Scanner lector = new Scanner(System.in);
      
      public void setMi(String msj){
	  Mi = msj;
      }
      public void setClave(String k){
	  clave = k;
      }
      
      public String getMi(){
	  return Mi;
      }
      
      public String getClave(){
	  return clave;
      }
      
      public void cifrar(){
	  String tmp = new String();
	  System.out.println("Introduzca el mensaje a cifrar SIN ESPACIOS:");
	  setMi(lector.nextLine());
	  System.out.println("Introduzca la clave para cifrar:");
	  setClave(lector.nextLine());
	  
	  for(int k = 0; k < 3; k++){
		    tmp = tmp.concat(clave);
	  }
	  // This would be the text we encrypt (in this case "hello")
	  // We convert it to a character array
	  char[] arText = Mi.toCharArray();

	  // This would be our vernam cipher (should be same length as our text)
	  // Here we use the same letters, but theoretically should be random
	  // characters generated on the fly. USE RANDOM LETTERS!;
	  char[] arCipher = tmp.toCharArray();

	  // Array to hold our encryption (again same length)
	  char[] temp = new char[Mi.length()];

	  // Encrypt the text by using XOR (exclusive OR) each character
	  // of our text against cipher.
	  System.out.println("Cifrando " + Mi + " a: ");
	  for (int i = 0; i < arText.length; i++) {
		  temp[i] =  (char) (arText[i] ^ arCipher[i]);
		  System.out.print(temp[i]);
	  }
	  System.out.println("\n");
      }
}

class DVernam{
      private String cipher;
      private String clave;
      Scanner lector = new Scanner(System.in);
      
      
      public void setCipher(String msj){
	  cipher = msj;
      }
      public void setClave(String k){
	  clave = k;
      }
      
      public String getCipher(){
	  return cipher;
      }
      
      public String getClave(){
	  return clave;
      }
      
      public void descifrar(){
	  String tmp = new String();
	  System.out.println("Introduzca el mensaje cifrado:");
	  setCipher(lector.nextLine());
	  System.out.println("Introduzca la clave para descifrar:");
	  setClave(lector.nextLine());
	  
	  for(int k = 0; k < 3; k++){
		    tmp = tmp.concat(clave);
	  }
	  // This would be the text we encrypt (in this case "hello")
	  // We convert it to a character array
	  char[] arClave = tmp.toCharArray();

	  // This would be our vernam cipher (should be same length as our text)
	  // Here we use the same letters, but theoretically should be random
	  // characters generated on the fly. USE RANDOM LETTERS!;
	  char[] arCipher = cipher.toCharArray();

	  // Array to hold our encryption (again same length)
	  char[] temp = new char[cipher.length()];

	  // Encrypt the text by using XOR (exclusive OR) each character
	  // of our text against cipher.
	  System.out.println("Descifrando " + cipher + " a: ");
	  for (int i = 0; i < arCipher.length; i++) {
		  temp[i] =  (char) (arClave[i] ^ arCipher[i]);
		  System.out.print(temp[i]);
	  }
	  System.out.println("\n");
      }
}