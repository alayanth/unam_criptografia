import java.util.*;

public class Vigenere {
    private String textoCifrado = "";
    private String textoClaro = "";
    CVigenere cifradoVigenere = new CVigenere();
    DVigenere descifradoVigenere = new DVigenere();
 
 
//     public void settextoCifrado(String Mi){
// 	textoCifrado = Mi;
//     }
//     
//     public String gettextoCifrado(){
// 	return textoCifrado;
//     }
//     
//     public void settextoClaro

    public String encriptarTextoClaro(String textoClaro, String clave) {
        String claveCompletada = "";
        int indice = 0;
        for (int i = indice; i < textoClaro.length(); i++) {
            for (int j = 0; j < clave.length(); j++) {
                if (claveCompletada.length() < textoClaro.length()) {
                    if (textoClaro.charAt(indice) != ' ') {
                        claveCompletada += clave.charAt(j) + "";
                    } else {
                        claveCompletada += " ";
                        j--;
                    }
                    indice++;
                }
            }
        }
 
        for (int i = 0; i < textoClaro.length(); i++) {
            char charTextoClaro = textoClaro.charAt(i);
            char charClaveCompletada = claveCompletada.charAt(i);
            if (charTextoClaro != ' ') {
                textoCifrado += cifradoVigenere.cifrado(charTextoClaro, charClaveCompletada) + "";
            } else {
                textoCifrado += " ";
            }
        }
 
        return textoCifrado;
    }
   
    public String desencriptarTextoCifrado(String textoCifrado, String clave) {
        String claveCompletada = "";
        int indice = 0;
        for (int i = indice; i < textoCifrado.length(); i++) {
            for (int j = 0; j < clave.length(); j++) {
                if (claveCompletada.length() < textoCifrado.length()) {
                    if (textoCifrado.charAt(indice) != ' ') {
                        claveCompletada += clave.charAt(j) + "";
                    } else {
                        claveCompletada += " ";
                        j--;
                    }
                    indice++;
                }
            }
        }
 
        for (int i = 0; i < textoCifrado.length(); i++) {
            char charTextoCifrado = textoCifrado.charAt(i);
            char charClaveCompletada = claveCompletada.charAt(i);
            if (charTextoCifrado != ' ') {
                textoClaro += descifradoVigenere.descifrado(charTextoCifrado, charClaveCompletada) + "";
            } else {
                textoClaro += " ";
            }
        }
 
        return textoClaro;    
    }
    
    public static void main(String[] args){
	String Mi = new String();
	String clave = new String();
	String cipher = new String();
	String temp = new String();
	Scanner lector = new Scanner(System.in);
	int option;
	
	
	Vigenere vigenere = new Vigenere();
	System.out.println("Universidad Nacional Autónoma de México");
	System.out.println("\tFacultad de Ingenieria");
	System.out.println("   Criptografía Cifrador Vigenere");
	System.out.println("Selecciona la opción que deseas ejecutar:\n"
			  +"1) Cifrar\n"
			  +"2) Descifrar");
			  
	option = lector.nextInt();
	lector.nextLine();

	    
	switch(option){
	  case 1:
	    System.out.print("Por favor introduce el mensaje a cifrar SIN ESPACIOS:");
	    System.out.print("\n");
	    Mi = lector.nextLine();
	    Mi = Mi.toUpperCase();
	    
	    System.out.println("Ahora la clave para cifrar:");
	    clave = lector.nextLine();
	    clave = clave.toUpperCase();
	    
	    for(int k = 0; k < 3; k++){
		    temp = temp.concat(clave);
	    }
	    System.out.println("Encriptando con Vigenere:");
	    System.out.println(vigenere.encriptarTextoClaro(Mi,clave));
	    break;
	  case 2:
	    System.out.println("Por favor introduce el mensaje cifrado:");
	    cipher = lector.nextLine();
	    cipher = cipher.toUpperCase();
	    
	    System.out.println("Ahora la clave para descifrar:");
	    clave = lector.nextLine();
	    clave = clave.toUpperCase();
	    
	    for(int k = 0; k < 3; k++){
		    temp = temp.concat(clave);
	    }
	    System.out.println("Decriptando con Vigenere:");
	    System.out.println(vigenere.desencriptarTextoCifrado(cipher,clave));
	    break;
	  default:
	      System.out.println("Opción incorrecta");
	    break;
	}/*
	System.out.println("Por favor introduce el mensaje a cifrar:");
	Mi = lector.nextLine();
	Mi = Mi.toUpperCase();
	
	System.out.println("Ahora la clave para cifrar:");
	clave = lector.nextLine();
	clave = clave.toUpperCase();
	
	for(int k = 0; k < 3; k++){
		temp = temp.concat(clave);
	}
	*/
    }
 
}