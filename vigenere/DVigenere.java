

public class DVigenere{
    char tablaCesar[] ={
    'A','B','C','D','E','F',
    'G','H','I','J','K','L',
    'M','N','Ñ','O','P','Q',
    'R','S','T','U','V','W',
    'X','Y','Z','0','1','2',
    '3','4','5','6','7','8',
    '9'};
    
    int n = 37;
    
    public char descifrado(char cipher, char clave){
	int indiceCipher = 0;
	int indiceClave = 0;
	
	
	for(int i = 0; i < tablaCesar.length; i++){
	    if(cipher == tablaCesar[i]){
	      indiceCipher = i;
	      break;
	    }
	}
	
	for(int j = 0; j < tablaCesar[j]; j++){
	    if(clave == tablaCesar[j]){
	      indiceClave = j;
	      break;
	    }
	    
	}
	
	if(indiceCipher >= indiceClave){
	    return tablaCesar[(indiceCipher - indiceClave) % n];
	}
	else{
	    return tablaCesar[n-(indiceClave - indiceCipher)];
	}
    }
}