import java.util.*;

public class CVigenere{
    char tablaCesar[] ={
    'A','B','C','D','E','F',
    'G','H','I','J','K','L',
    'M','N','Ñ','O','P','Q',
    'R','S','T','U','V','W',
    'X','Y','Z','0','1','2',
    '3','4','5','6','7','8',
    '9'};
    
    int n = 37;
    
    public char cifrado(char Mi, char clave){
	int xVal = 0;
	int yVal = 0;
	
	for(int i = 0; i < tablaCesar.length; i++){
	    if(Mi == tablaCesar[i]){
	      xVal = i;
	      break;
	    }
	}
	
	for(int j = 0; j < tablaCesar[j]; j++){
	    if(clave == tablaCesar[j]){
	      yVal = j;
	      break;
	    }
	    
	}
	
	return tablaCesar[(xVal + yVal) % n];
    }
    
}